<?php
    namespace App\Controller;

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\Routing\Annotation\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

    class EmailTemplateController extends AbstractController {
        /**
         * @Route("/")
         */
        public function index() {
            
    //         Usuario ################################################

    //         NEW_PARTNER_APP
    //         Cadastro de um novo aplicativo pelo seja-parceiro, o usuario recebe a notificação.
            
            // return $this->render('emails/apps/usuarios/new_partner_app.html.twig',['obj'=> 
            // [ 
            //     "app" => [
            //         "url" => "https:\\\\www.nutror.com.br",
            //         "name" => "Nutror",
            //         "description" => "Aplicativo teste"
            //     ], 
            //     "partner" => [
            //         "email" => "Luiz Rocha",
            //         "phone" => "14981334567",
            //         "name" => "Luiz Rocha",
            //         "companyName" => "LCompany"
            //     ]
            //     ]]);
            
                //         NEW_PARTNER_PLUGIN
                //  Cadastro de um novo PLUGIN pelo seja-parceiro, o usuario recebe a notificação.
            
                // return $this->render('emails/plugins/usuarios/new_partner_plugin.html.twig',['obj'=> 
                // [ 
                //     "plugin" => [
                //         "url" => "https:\\\\www.nutror.com.br",
                //         "name" => "Nutror",
                //         "description" => "Aplicativo teste"
                //     ], 
                //     "partner" => [
                //         "email" => "Luiz Rocha",
                //         "phone" => "14981334567",
                //         "name" => "Luiz Rocha",
                //         "companyName" => "LCompany"
                //     ]
                //     ]]);

    //         NEW_COMMENT

    //         return $this->render('emails/apps/usuarios/new_comment.html.twig',['obj'=> 
    // [ 
    //             "id" => "bd2a92ff-183a-45bf-b52e-99889946ca47",
    //             "app" => [
    //                 "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
    //                 "name" => "HighTechWare",
    //                 "partnerName" => "Software4Sales",
    //                 "review" => 4,
    //                 "numberOfReviews" => 2,
    //                 "slug" => "hightechware",
    //                 "totalComments" => 2
    //             ], 
    //             "comment" => [
    //                 "author" => [
    //                     "id" => "123",
    //                     "name" => "Luiz Rocha",
    //                     "firstLetter" => "L"
    //                 ],
    //                 "created_At" => "2021-06-07T12:32:41.8553043Z",
    //                 "fullDate" => "7 de junho de 2021 | 12:32",
    //                 "updated_At" => "2021-06-14T13:47:00.3189295Z",
    //                 "review" => 4,
    //                 "title" => "teste123",
    //                 "description" => "teste123 ",
    //                 "status" => "Approved",
    //                 "likes" => 0,
    //                 "reply" => [
    //                     "1" => [
    //                         "id" => "60023b2a-40ba-48e7-a4b2-c609660e62e3",
    //                         "description" => "resposta usuario nova",
    //                         "isDraft" => false,
    //                         "author" => null,
    //                         "created_At" => "0001-01-01T00:00:00Z",
    //                         "updated_At" => "2021-06-07T12:42:46.5283074Z",
    //                         "fullDate" => "7 de junho de 2021 | 12:32",
    //                         "isPartner" => true
    //                     ],
    //                     "2" => [
    //                         "id" => "a3e31c89-88c8-4bd5-859b-eee4bd158a7a",
    //                         "description" => "resposta colaborador!",
    //                         "isDraft" => false,
    //                         "author" => null,
    //                         "created_At" => "0001-01-01T00:00:00Z",
    //                         "updated_At" => "2021-06-07T12:39:18.3317356Z",
    //                         "fullDate" => "7 de junho de 2021 | 12:32",
    //                         "isPartner" => false
    //                     ]
    //                 ]
                    
    //             ]
    //         ]
    //     ]);


    //         NEW_COMMENT_PLUGIN

    //         return $this->render('emails/plugins/usuarios/new_comment_plugin.html.twig',['obj'=> 
    // [ 
    //             "id" => "bd2a92ff-183a-45bf-b52e-99889946ca47",
    //             "plugin" => [
    //                 "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
    //                 "name" => "HighTechWare",
    //                 "partnerName" => "Software4Sales",
    //                 "review" => 4,
    //                 "numberOfReviews" => 2,
    //                 "slug" => "hightechware",
    //                 "totalComments" => 2
    //             ], 
    //             "comment" => [
    //                 "author" => [
    //                     "id" => "123",
    //                     "name" => "Luiz Rocha",
    //                     "firstLetter" => "L"
    //                 ],
    //                 "created_At" => "2021-06-07T12:32:41.8553043Z",
    //                 "fullDate" => "7 de junho de 2021 | 12:32",
    //                 "updated_At" => "2021-06-14T13:47:00.3189295Z",
    //                 "review" => 4,
    //                 "title" => "teste123",
    //                 "description" => "teste123 ",
    //                 "status" => "Approved",
    //                 "likes" => 0,
    //                 "reply" => [
    //                     "1" => [
    //                         "id" => "60023b2a-40ba-48e7-a4b2-c609660e62e3",
    //                         "description" => "resposta usuario nova",
    //                         "isDraft" => false,
    //                         "author" => null,
    //                         "created_At" => "0001-01-01T00:00:00Z",
    //                         "updated_At" => "2021-06-07T12:42:46.5283074Z",
    //                         "fullDate" => "7 de junho de 2021 | 12:32",
    //                         "isPartner" => true
    //                     ],
    //                     "2" => [
    //                         "id" => "a3e31c89-88c8-4bd5-859b-eee4bd158a7a",
    //                         "description" => "resposta colaborador!",
    //                         "isDraft" => false,
    //                         "author" => null,
    //                         "created_At" => "0001-01-01T00:00:00Z",
    //                         "updated_At" => "2021-06-07T12:39:18.3317356Z",
    //                         "fullDate" => "7 de junho de 2021 | 12:32",
    //                         "isPartner" => false
    //                     ]
    //                 ]
                    
    //             ]
    //         ]
    //     ]);


        //     // NEW_COMMENT_APPROVED

        //     return $this->render('emails/apps/usuarios/new_comment_approved.html.twig',['obj'=> 
        // [ 
        //             "id" => "bd2a92ff-183a-45bf-b52e-99889946ca47",
        //             "spaceUrl" => "https://space-qa.testzz.ninja/",
        //             "app" => [
        //                 "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
        //                 "name" => "HighTechWare",
        //                 "partnerName" => "Software4Sales",
        //                 "review" => 4,
        //                 "numberOfReviews" => 2,
        //                 "slug" => "hightechware",
        //                 "totalComments" => 2
        //             ], 
        //             "comment" => [
        //                 "author" => [
        //                     "id" => "123",
        //                     "name" => "Luiz Rocha",
        //                     "firstLetter" => "L"
        //                 ],
        //                 "created_At" => "2021-06-07T12:32:41.8553043Z",
        //                 "fullDate" => "7 de junho de 2021 | 12:32",
        //                 "updated_At" => "2021-06-14T13:47:00.3189295Z",
        //                 "review" => 4,
        //                 "title" => "teste123",
        //                 "description" => "teste123 ",
        //                 "status" => "Approved",
        //                 "likes" => 0,
        //                 "reply" => [
        //                     "1" => [
        //                         "id" => "a3e31c89-88c8-4bd5-859b-eee4bd158a7a",
        //                         "description" => "resposta colaborador!",
        //                         "isDraft" => false,
        //                         "author" => null,
        //                         "created_At" => "0001-01-01T00:00:00Z",
        //                         "updated_At" => "2021-06-07T12:39:18.3317356Z",
        //                         "fullDate" => "7 de junho de 2021 | 12:32",
        //                         "isPartner" => false
        //                     ]
        //                 ]
                        
        //             ]
        //         ]
        //     ]);

          //     // NEW_COMMENT_APPROVED_PLUGIN

        //     return $this->render('emails/plugins/usuarios/new_comment_approved_plugin.html.twig',['obj'=> 
        // [ 
        //             "id" => "bd2a92ff-183a-45bf-b52e-99889946ca47",
        //             "spaceUrl" => "https://space-qa.testzz.ninja/",
        //             "plugin" => [
        //                 "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
        //                 "name" => "HighTechWare",
        //                 "partnerName" => "Software4Sales",
        //                 "review" => 4,
        //                 "numberOfReviews" => 2,
        //                 "slug" => "hightechware",
        //                 "totalComments" => 2
        //             ], 
        //             "comment" => [
        //                 "author" => [
        //                     "id" => "123",
        //                     "name" => "Luiz Rocha",
        //                     "firstLetter" => "L"
        //                 ],
        //                 "created_At" => "2021-06-07T12:32:41.8553043Z",
        //                 "fullDate" => "7 de junho de 2021 | 12:32",
        //                 "updated_At" => "2021-06-14T13:47:00.3189295Z",
        //                 "review" => 4,
        //                 "title" => "teste123",
        //                 "description" => "teste123 ",
        //                 "status" => "Approved",
        //                 "likes" => 0,
        //                 "reply" => [
        //                     "1" => [
        //                         "id" => "a3e31c89-88c8-4bd5-859b-eee4bd158a7a",
        //                         "description" => "resposta colaborador!",
        //                         "isDraft" => false,
        //                         "author" => null,
        //                         "created_At" => "0001-01-01T00:00:00Z",
        //                         "updated_At" => "2021-06-07T12:39:18.3317356Z",
        //                         "fullDate" => "7 de junho de 2021 | 12:32",
        //                         "isPartner" => false
        //                     ]
        //                 ]
                        
        //             ]
        //         ]
        //     ]);


                // NEW_COMMENT_REPROVED

        //     return $this->render('emails/apps/usuarios/new_comment_reproved.html.twig',['obj'=> 
        // [ 
        //             "id" => "bd2a92ff-183a-45bf-b52e-99889946ca47",
        //             "app" => [
        //                 "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
        //                 "name" => "HighTechWare",
        //                 "partnerName" => "Software4Sales",
        //                 "review" => 4,
        //                 "numberOfReviews" => 2,
        //                 "slug" => "hightechware",
        //                 "totalComments" => 2
        //             ], 
        //             "comment" => [
        //                 "author" => [
        //                     "id" => "123",
        //                     "name" => "Luiz Rocha",
        //                     "firstLetter" => "L"
        //                 ],
        //                 "created_At" => "2021-06-07T12:32:41.8553043Z",
        //                 "fullDate" => "7 de junho de 2021 | 12:32",
        //                 "updated_At" => "2021-06-14T13:47:00.3189295Z",
        //                 "review" => 4,
        //                 "title" => "teste123",
        //                 "description" => "teste123 ",
        //                 "status" => "Approved",
        //                 "likes" => 0,
        //                 "reply" => [
        //                     "1" => [
        //                         "id" => "60023b2a-40ba-48e7-a4b2-c609660e62e3",
        //                         "description" => "resposta usuario nova",
        //                         "isDraft" => false,
        //                         "author" => null,
        //                         "created_At" => "0001-01-01T00:00:00Z",
        //                         "updated_At" => "2021-06-07T12:42:46.5283074Z",
        //                         "fullDate" => "7 de junho de 2021 | 12:32",
        //                         "isPartner" => true
        //                     ],
        //                     "2" => [
        //                         "id" => "a3e31c89-88c8-4bd5-859b-eee4bd158a7a",
        //                         "description" => "resposta colaborador!",
        //                         "isDraft" => false,
        //                         "author" => null,
        //                         "created_At" => "0001-01-01T00:00:00Z",
        //                         "updated_At" => "2021-06-07T12:39:18.3317356Z",
        //                         "fullDate" => "7 de junho de 2021 | 12:32",
        //                         "isPartner" => false
        //                     ]
        //                 ]
                        
        //             ]
        //         ]
        //     ]);

        // NEW_COMMENT_REPROVED_PLUGIN

        //     return $this->render('emails/plugins/usuarios/new_comment_reproved_plugin.html.twig',['obj'=> 
        // [ 
        //             "id" => "bd2a92ff-183a-45bf-b52e-99889946ca47",
        //             "spaceUrl" => "https://space-qa.testzz.ninja/",
        //             "plugin" => [
        //                 "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
        //                 "name" => "HighTechWare",
        //                 "partnerName" => "Software4Sales",
        //                 "review" => 4,
        //                 "numberOfReviews" => 2,
        //                 "slug" => "hightechware",
        //                 "totalComments" => 2
        //             ], 
        //             "comment" => [
        //                 "author" => [
        //                     "id" => "123",
        //                     "name" => "Luiz Rocha",
        //                     "firstLetter" => "L"
        //                 ],
        //                 "created_At" => "2021-06-07T12:32:41.8553043Z",
        //                 "fullDate" => "7 de junho de 2021 | 12:32",
        //                 "updated_At" => "2021-06-14T13:47:00.3189295Z",
        //                 "review" => 4,
        //                 "title" => "teste123",
        //                 "description" => "teste123 ",
        //                 "status" => "Approved",
        //                 "likes" => 0,
        //                 "reply" => [
        //                     "1" => [
        //                         "id" => "a3e31c89-88c8-4bd5-859b-eee4bd158a7a",
        //                         "description" => "resposta colaborador!",
        //                         "isDraft" => false,
        //                         "author" => null,
        //                         "created_At" => "0001-01-01T00:00:00Z",
        //                         "updated_At" => "2021-06-07T12:39:18.3317356Z",
        //                         "fullDate" => "7 de junho de 2021 | 12:32",
        //                         "isPartner" => false
        //                     ]
        //                 ]
                        
        //             ]
        //         ]
        //     ]);


        // NEW_COMMENT_TOREVIEW

        // return $this->render('emails/apps/usuarios/new_comment_toreview.html.twig',['obj'=> 
        // [ 
            //         "id" => "bd2a92ff-183a-45bf-b52e-99889946ca47",
            //         "app" => [
            //             "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
            //             "name" => "HighTechWare",
            //             "partnerName" => "Software4Sales",
            //             "review" => 4,
            //             "numberOfReviews" => 2,
            //             "slug" => "hightechware",
            //             "totalComments" => 2
            //         ], 
            //         "comment" => [
            //             "author" => [
            //                 "id" => "123",
            //                 "name" => "Luiz Rocha",
            //                 "firstLetter" => "L"
            //             ],
            //             "created_At" => "2021-06-07T12:32:41.8553043Z",
            //             "fullDate" => "7 de junho de 2021 | 12:32",
            //             "updated_At" => "2021-06-14T13:47:00.3189295Z",
            //             "review" => 4,
            //             "title" => "teste123",
            //             "description" => "teste123 ",
            //             "status" => "Approved",
            //             "likes" => 0,
            //             "reply" => [
            //                 "1" => [
            //                     "id" => "60023b2a-40ba-48e7-a4b2-c609660e62e3",
            //                     "description" => "resposta usuario nova",
            //                     "isDraft" => false,
            //                     "author" => null,
            //                     "created_At" => "0001-01-01T00:00:00Z",
            //                     "updated_At" => "2021-06-07T12:42:46.5283074Z",
            //                     "fullDate" => "7 de junho de 2021 | 12:32",
            //                     "isPartner" => true
            //                 ],
            //                 "2" => [
            //                     "id" => "a3e31c89-88c8-4bd5-859b-eee4bd158a7a",
            //                     "description" => "resposta colaborador!",
            //                     "isDraft" => false,
            //                     "author" => null,
            //                     "created_At" => "0001-01-01T00:00:00Z",
            //                     "updated_At" => "2021-06-07T12:39:18.3317356Z",
            //                     "fullDate" => "7 de junho de 2021 | 12:32",
            //                     "isPartner" => false
            //                 ]
            //             ]
                        
            //         ]
            //     ]
            // ]);

             // NEW_COMMENT_TOREVIEW_PLUGIN

        // return $this->render('emails/plugins/usuarios/new_comment_toreview_plugin.html.twig',['obj'=> 
        // [ 
        //             "id" => "bd2a92ff-183a-45bf-b52e-99889946ca47",
        //             "plugin" => [
        //                 "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
        //                 "name" => "HighTechWare",
        //                 "partnerName" => "Software4Sales",
        //                 "review" => 4,
        //                 "numberOfReviews" => 2,
        //                 "slug" => "hightechware",
        //                 "totalComments" => 2
        //             ], 
        //             "comment" => [
        //                 "author" => [
        //                     "id" => "123",
        //                     "name" => "Luiz Rocha",
        //                     "firstLetter" => "L"
        //                 ],
        //                 "created_At" => "2021-06-07T12:32:41.8553043Z",
        //                 "fullDate" => "7 de junho de 2021 | 12:32",
        //                 "updated_At" => "2021-06-14T13:47:00.3189295Z",
        //                 "review" => 4,
        //                 "title" => "teste123",
        //                 "description" => "teste123 ",
        //                 "status" => "Approved",
        //                 "likes" => 0,
        //                 "reply" => [
        //                     "1" => [
        //                         "id" => "60023b2a-40ba-48e7-a4b2-c609660e62e3",
        //                         "description" => "resposta usuario nova",
        //                         "isDraft" => false,
        //                         "author" => null,
        //                         "created_At" => "0001-01-01T00:00:00Z",
        //                         "updated_At" => "2021-06-07T12:42:46.5283074Z",
        //                         "fullDate" => "7 de junho de 2021 | 12:32",
        //                         "isPartner" => true
        //                     ],
        //                     "2" => [
        //                         "id" => "a3e31c89-88c8-4bd5-859b-eee4bd158a7a",
        //                         "description" => "resposta colaborador!",
        //                         "isDraft" => false,
        //                         "author" => null,
        //                         "created_At" => "0001-01-01T00:00:00Z",
        //                         "updated_At" => "2021-06-07T12:39:18.3317356Z",
        //                         "fullDate" => "7 de junho de 2021 | 12:32",
        //                         "isPartner" => false
        //                     ]
        //                 ]
                        
        //             ]
        //         ]
        //     ]);


            // NEW_APP_PUBLISHED

            //  return $this->render('emails/apps/usuarios/new_app_published.html.twig',['obj'=> 
            //     [ 
            //             "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
            //             "name" => "HighTechWare",
            //             "companyName" => "Software4Sales",
            //             "partnerName" => "Frida Barros",
            //             "slug" => "nutror",
            //             "spaceUrl" => ""
            //         ], 
            // ]);

             // NEW_PLUGIN_PUBLISHED

            //  return $this->render('emails/plugins/usuarios/new_plugin_published.html.twig',['obj'=> 
            //     [ 
            //             "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
            //             "name" => "HighTechWare",
            //             "companyName" => "Software4Sales",
            //             "partnerName" => "Frida Barros",
            //             "slug" => "nutror",
            //             "spaceUrl" => ""
            //         ], 
            // ]);

    // NEW_REPLY_EDUZZ
        // Quando o comentario do usuario é respondido pelo colaborador, o usuario recebe a notificação

    //     return $this->render('emails/apps/usuarios/new_reply_eduzz.html.twig',['obj'=> 
    // [ 
    //             "id" => "bd2a92ff-183a-45bf-b52e-99889946ca47",
    //             "app" => [
    //                 "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
    //                 "name" => "HighTechWare",
    //                 "partnerName" => "Software4Sales",
    //                 "review" => 4,
    //                 "numberOfReviews" => 2,
    //                 "slug" => "hightechware",
    //                 "totalComments" => 2
    //             ], 
    //             "comment" => [
    //                 "author" => [
    //                     "id" => "123",
    //                     "name" => "Luiz Rocha",
    //                     "firstLetter" => "L"
    //                 ],
    //                 "created_At" => "2021-06-07T12:32:41.8553043Z",
    //                 "fullDate" => "7 de junho de 2021 | 12:32",
    //                 "updated_At" => "2021-06-14T13:47:00.3189295Z",
    //                 "review" => 4,
    //                 "title" => "teste123",
    //                 "description" => "teste123 ",
    //                 "status" => "Approved",
    //                 "likes" => 0,
    //                 "reply" => [
    //                     "1" => [
    //                         "id" => "60023b2a-40ba-48e7-a4b2-c609660e62e3",
    //                         "description" => "resposta usuario nova",
    //                         "isDraft" => false,
    //                         "author" => null,
    //                         "created_At" => "0001-01-01T00:00:00Z",
    //                         "updated_At" => "2021-06-07T12:42:46.5283074Z",
    //                         "fullDate" => "7 de junho de 2021 | 12:32",
    //                         "isPartner" => true
    //                     ],
    //                     "2" => [
    //                         "id" => "a3e31c89-88c8-4bd5-859b-eee4bd158a7a",
    //                         "description" => "resposta colaborador!",
    //                         "isDraft" => false,
    //                         "author" => null,
    //                         "created_At" => "0001-01-01T00:00:00Z",
    //                         "updated_At" => "2021-06-07T12:39:18.3317356Z",
    //                         "fullDate" => "7 de junho de 2021 | 12:32",
    //                         "isPartner" => false
    //                     ]
    //                 ]
                    
    //             ]
    //         ]
    //     ]);

    //  NEW_REPLY_EDUZZ_PLUGIN
    //     Quando o comentario do usuario é respondido pelo colaborador, o usuario recebe a notificação

    //     return $this->render('emails/plugins/usuarios/new_reply_eduzz_plugin.html.twig',['obj'=> 
    // [ 
    //             "id" => "bd2a92ff-183a-45bf-b52e-99889946ca47",
    //             "plugin" => [
    //                 "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
    //                 "name" => "HighTechWare",
    //                 "partnerName" => "Software4Sales",
    //                 "review" => 4,
    //                 "numberOfReviews" => 2,
    //                 "slug" => "hightechware",
    //                 "totalComments" => 2
    //             ], 
    //             "comment" => [
    //                 "author" => [
    //                     "id" => "123",
    //                     "name" => "Luiz Rocha",
    //                     "firstLetter" => "L"
    //                 ],
    //                 "created_At" => "2021-06-07T12:32:41.8553043Z",
    //                 "fullDate" => "7 de junho de 2021 | 12:32",
    //                 "updated_At" => "2021-06-14T13:47:00.3189295Z",
    //                 "review" => 4,
    //                 "title" => "teste123",
    //                 "description" => "teste123 ",
    //                 "status" => "Approved",
    //                 "likes" => 0,
    //                 "reply" => [
    //                         "id" => "60023b2a-40ba-48e7-a4b2-c609660e62e3",
    //                         "description" => "resposta usuario nova",
    //                         "isDraft" => false,
    //                         "author" => null,
    //                         "created_At" => "0001-01-01T00:00:00Z",
    //                         "updated_At" => "2021-06-07T12:42:46.5283074Z",
    //                         "fullDate" => "7 de junho de 2021 | 12:32",
    //                         "isPartner" => true
    //                 ]
                    
    //             ]
    //         ]
    //     ]);

    // NEW_REPLY_PARTNER
        // Quando o comentario do usuario é respondido pelo parceiro/dono do aplicativo, o usuario recebe a notificação

    //     return $this->render('emails/apps/usuarios/new_reply_partner.html.twig',['obj'=> 
    // [ 
    //             "id" => "bd2a92ff-183a-45bf-b52e-99889946ca47",
    //             "app" => [
    //                 "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
    //                 "name" => "HighTechWare",
    //                 "partnerName" => "Software4Sales",
    //                 "review" => 4,
    //                 "numberOfReviews" => 2,
    //                 "slug" => "hightechware",
    //                 "totalComments" => 2
    //             ], 
    //             "comment" => [
    //                 "author" => [
    //                     "id" => "123",
    //                     "name" => "Luiz Rocha",
    //                     "firstLetter" => "L"
    //                 ],
    //                 "created_At" => "2021-06-07T12:32:41.8553043Z",
    //                 "fullDate" => "7 de junho de 2021 | 12:32",
    //                 "updated_At" => "2021-06-14T13:47:00.3189295Z",
    //                 "review" => 4,
    //                 "title" => "teste123",
    //                 "description" => "teste123 ",
    //                 "status" => "Approved",
    //                 "likes" => 0,
    //                 "reply" => [
    //                         "id" => "60023b2a-40ba-48e7-a4b2-c609660e62e3",
    //                         "description" => "resposta usuario nova",
    //                         "isDraft" => false,
    //                         "author" => null,
    //                         "created_At" => "0001-01-01T00:00:00Z",
    //                         "updated_At" => "2021-06-07T12:42:46.5283074Z",
    //                         "fullDate" => "7 de junho de 2021 | 12:32",
    //                         "isPartner" => true
    //                 ]
                    
    //             ]
    //         ]
    //     ]);

    // NEW_REPLY_PARTNER_PLUGIN
        // Quando o comentario do usuario é respondido pelo parceiro/dono do aplicativo, o usuario recebe a notificação

    //     return $this->render('emails/plugins/usuarios/new_reply_partner_plugin.html.twig',['obj'=> 
    // [ 
    //             "id" => "bd2a92ff-183a-45bf-b52e-99889946ca47",
    //             "plugin" => [
    //                 "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
    //                 "name" => "HighTechWare",
    //                 "partnerName" => "Software4Sales",
    //                 "review" => 4,
    //                 "numberOfReviews" => 2,
    //                 "slug" => "hightechware",
    //                 "totalComments" => 2
    //             ], 
    //             "comment" => [
    //                 "author" => [
    //                     "id" => "123",
    //                     "name" => "Luiz Rocha",
    //                     "firstLetter" => "L"
    //                 ],
    //                 "created_At" => "2021-06-07T12:32:41.8553043Z",
    //                 "fullDate" => "7 de junho de 2021 | 12:32",
    //                 "updated_At" => "2021-06-14T13:47:00.3189295Z",
    //                 "review" => 4,
    //                 "title" => "teste123",
    //                 "description" => "teste123 ",
    //                 "status" => "Approved",
    //                 "likes" => 0,
    //                 "reply" => [
    //                         "id" => "60023b2a-40ba-48e7-a4b2-c609660e62e3",
    //                         "description" => "resposta usuario nova",
    //                         "isDraft" => false,
    //                         "author" => null,
    //                         "created_At" => "0001-01-01T00:00:00Z",
    //                         "updated_At" => "2021-06-07T12:42:46.5283074Z",
    //                         "fullDate" => "7 de junho de 2021 | 12:32",
    //                         "isPartner" => true
    //                 ]
                    
    //             ]
    //         ]
    //     ]);

            // NEW_COMMENT_EDITED
            // Quando o comentario do usuario é modificado/editado pelo colaborador da Eduzz durante a moderação, o usuario recebe a notificação.

    //     return $this->render('emails/apps/usuarios/new_comment_edited.html.twig',['obj'=> 
    // [ 
    //             "id" => "9b3b5958-2cef-49d4-b64a-934effacaa87",
    //             "app" => [
    //                 "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
    //                 "name" => "HighTechWare",
    //                 "partnerName" => "Software4Sales",
    //                 "review" => 4,
    //                 "numberOfReviews" => 2,
    //                 "slug" => "string",
    //                 "totalComments" => 2
    //             ], 
    //             "comment" => [
    //                 "author" => [
    //                     "id" => "123",
    //                     "name" => "Luiz Rocha",
    //                     "firstLetter" => "L"
    //                 ],
    //                 "created_At" => "2021-06-07T12:32:41.8553043Z",
    //                 "fullDate" => "7 de junho de 2021 | 12:32",
    //                 "updated_At" => "2021-06-14T13:47:00.3189295Z",
    //                 "review" => 4,
    //                 "title" => "teste $#$@# em",
    //                 "description" => "teste123 teste 123 teste teste",
    //                 "status" => "Approved",
    //                 "likes" => 0,
    //                 "reply" => [
    //                     "1" => [
    //                         "id" => "60023b2a-40ba-48e7-a4b2-c609660e62e3",
    //                         "description" => "resposta usuario nova",
    //                         "isDraft" => false,
    //                         "author" => null,
    //                         "created_At" => "0001-01-01T00:00:00Z",
    //                         "updated_At" => "2021-06-07T12:42:46.5283074Z",
    //                         "fullDate" => "7 de junho de 2021 | 12:32",
    //                         "isPartner" => true
    //                     ],
    //                     "2" => [
    //                         "id" => "a3e31c89-88c8-4bd5-859b-eee4bd158a7a",
    //                         "description" => "resposta colaborador!",
    //                         "isDraft" => false,
    //                         "author" => null,
    //                         "created_At" => "0001-01-01T00:00:00Z",
    //                         "updated_At" => "2021-06-07T12:39:18.3317356Z",
    //                         "fullDate" => "7 de junho de 2021 | 12:32",
    //                         "isPartner" => false
    //                     ]
    //                     ],
    //                     "oldComment" => [
    //                         "review" => 4,
    //                         "title" => "teste123 foda em!",
    //                         "description" => "teste123 teste 123 teste teste"
    //                     ]
                    
    //             ]
    //         ]
    //     ]);

             // NEW_COMMENT_EDITED_PLUGIN
            // Quando o comentario do usuario é modificado/editado pelo colaborador da Eduzz durante a moderação, o usuario recebe a notificação.

    //     return $this->render('emails/plugins/usuarios/new_comment_edited_plugin.html.twig',['obj'=> 
    // [ 
    //             "id" => "9b3b5958-2cef-49d4-b64a-934effacaa87",
    //             "spaceUrl" => "http://space-qa.testzz.ninja/",
    //             "plugin" => [
    //                 "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
    //                 "name" => "HighTechWare",
    //                 "partnerName" => "Software4Sales",
    //                 "review" => 4,
    //                 "numberOfReviews" => 2,
    //                 "slug" => "string",
    //                 "totalComments" => 2
    //             ], 
    //             "comment" => [
    //                 "author" => [
    //                     "id" => "123",
    //                     "name" => "Luiz Rocha",
    //                     "firstLetter" => "L"
    //                 ],
    //                 "created_At" => "2021-06-07T12:32:41.8553043Z",
    //                 "fullDate" => "7 de junho de 2021 | 12:32",
    //                 "updated_At" => "2021-06-14T13:47:00.3189295Z",
    //                 "review" => 4,
    //                 "title" => "teste $#$@# em",
    //                 "description" => "teste123 teste 123 teste teste",
    //                 "status" => "Approved",
    //                 "likes" => 0,
    //                 "reply" => [
    //                     "1" => [
    //                         "id" => "60023b2a-40ba-48e7-a4b2-c609660e62e3",
    //                         "description" => "resposta usuario nova",
    //                         "isDraft" => false,
    //                         "author" => null,
    //                         "created_At" => "0001-01-01T00:00:00Z",
    //                         "updated_At" => "2021-06-07T12:42:46.5283074Z",
    //                         "fullDate" => "7 de junho de 2021 | 12:32",
    //                         "isPartner" => true
    //                     ],
    //                     "2" => [
    //                         "id" => "a3e31c89-88c8-4bd5-859b-eee4bd158a7a",
    //                         "description" => "resposta colaborador!",
    //                         "isDraft" => false,
    //                         "author" => null,
    //                         "created_At" => "0001-01-01T00:00:00Z",
    //                         "updated_At" => "2021-06-07T12:39:18.3317356Z",
    //                         "fullDate" => "7 de junho de 2021 | 12:32",
    //                         "isPartner" => false
    //                     ]
    //                     ],
    //                     "oldComment" => [
    //                         "review" => 4,
    //                         "title" => "teste123 foda em!",
    //                         "description" => "teste123 teste 123 teste teste"
    //                     ]
                    
    //             ]
    //         ]
    //     ]);


    //         Colaborador ################################################

    //         NEW_PARTNER_APP_EDUZZ
    //         Notificação que o colaborador da eduzz recebe quando um novo aplicativo é enviado pelo seja-parceiro, o colaborador receba a notificação.

            // return $this->render('emails/apps/colaborador/new_partner_app_eduzz.html.twig',['obj'=> 
            // [ 
            //     "app" => [
            //         "url" => "https:\\\\www.nutror.com.br",
            //         "name" => "Nutror",
            //         "description" => "Aplicativo teste"
            //     ], 
            //     "partner" => [
            //         "email" => "Luiz Rocha",
            //         "phone" => "14981334567",
            //         "name" => "Luiz Rocha",
            //         "companyName" => "LCompany"
            //     ]
            // ]]);

     //         NEW_PARTNER_PLUGIN_EDUZZ
    //         Notificação que o colaborador da eduzz recebe quando um novo aplicativo é enviado pelo seja-parceiro, o colaborador receba a notificação.

            // return $this->render('emails/plugins/colaborador/new_partner_plugin_eduzz.html.twig',['obj'=> 
            // [ 
            //     "plugin" => [
            //         "url" => "https:\\\\www.nutror.com.br",
            //         "name" => "Nutror",
            //         "description" => "Aplicativo teste"
            //     ], 
            //     "partner" => [
            //         "email" => "Luiz Rocha",
            //         "phone" => "14981334567",
            //         "name" => "Luiz Rocha",
            //         "companyName" => "LCompany"
            //     ]
            // ]]);

    //         NEW_REPLY_PARTNER_EDUZZ
    //         Quando o parceiro do aplicativo responde o comentario do usuario, o colaborador da eduzz é notificado dessa resposta também.

        //     return $this->render('emails/apps/colaborador/new_reply_partner_eduzz.html.twig',['obj'=> 
        //     [ 
        //         "id" => "bd2a92ff-183a-45bf-b52e-99889946ca47",
        //         "app" => [
        //             "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
        //             "name" => "HighTechWare",
        //             "partnerName" => "Software4Sales",
        //             "review" => 4,
        //             "numberOfReviews" => 2,
        //             "slug" => "hightechware",
        //             "totalComments" => 2
        //         ], 
        //         "comment" => [
        //             "author" => [
        //                 "id" => "123",
        //                 "name" => "Luiz Rocha",
        //                 "firstLetter" => "L"
        //             ],
        //             "created_At" => "2021-06-07T12:32:41.8553043Z",
        //             "fullDate" => "7 de junho de 2021 | 12:32",
        //             "updated_At" => "2021-06-14T13:47:00.3189295Z",
        //             "review" => 4,
        //             "title" => "teste123",
        //             "description" => "teste123 ",
        //             "status" => "Approved",
        //             "likes" => 0,
        //             "reply" => [
        //                     "id" => "60023b2a-40ba-48e7-a4b2-c609660e62e3",
        //                     "description" => "resposta usuario nova",
        //                     "isDraft" => false,
        //                     "author" => null,
        //                     "created_At" => "0001-01-01T00:00:00Z",
        //                     "updated_At" => "2021-06-07T12:42:46.5283074Z",
        //                     "fullDate" => "7 de junho de 2021 | 12:32",
        //                     "isPartner" => true
        //             ]
                    
        //         ]
        //     ]
        // ]);

         //         NEW_REPLY_PARTNER_EDUZZ_PLUGIN
    //         Quando o parceiro do aplicativo responde o comentario do usuario, o colaborador da eduzz é notificado dessa resposta também.

        //     return $this->render('emails/plugins/colaborador/new_reply_partner_eduzz_plugin.html.twig',['obj'=> 
        //     [ 
        //         "id" => "bd2a92ff-183a-45bf-b52e-99889946ca47",
        //         "plugin" => [
        //             "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
        //             "name" => "HighTechWare",
        //             "partnerName" => "Software4Sales",
        //             "review" => 4,
        //             "numberOfReviews" => 2,
        //             "slug" => "hightechware",
        //             "totalComments" => 2
        //         ], 
        //         "comment" => [
        //             "author" => [
        //                 "id" => "123",
        //                 "name" => "Luiz Rocha",
        //                 "firstLetter" => "L"
        //             ],
        //             "created_At" => "2021-06-07T12:32:41.8553043Z",
        //             "fullDate" => "7 de junho de 2021 | 12:32",
        //             "updated_At" => "2021-06-14T13:47:00.3189295Z",
        //             "review" => 4,
        //             "title" => "teste123",
        //             "description" => "teste123 ",
        //             "status" => "Approved",
        //             "likes" => 0,
        //             "reply" => [
        //                     "id" => "60023b2a-40ba-48e7-a4b2-c609660e62e3",
        //                     "description" => "resposta usuario nova",
        //                     "isDraft" => false,
        //                     "author" => null,
        //                     "created_At" => "0001-01-01T00:00:00Z",
        //                     "updated_At" => "2021-06-07T12:42:46.5283074Z",
        //                     "fullDate" => "7 de junho de 2021 | 12:32",
        //                     "isPartner" => true
        //             ]
                    
        //         ]
        //     ]
        // ]);


    //         Parceiro ################################################

    //         NEW_COMMENT_APPROVED_PARTNER
    //         Quando um comentario é aprovado, o parceiro tem acesso para responder este comentario. Sendo assim, quando o comentario for aprovado, o parceiro tem que ser notificado sobre o comentario.


    //         return $this->render('emails/apps/parceiro/new_comment_approved_partner.html.twig',['obj'=> 
    //     [ 
    //         "id" => "bd2a92ff-183a-45bf-b52e-99889946ca47",
    //         "app" => [
    //             "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
    //             "name" => "HighTechWare",
    //             "partnerName" => "Software4Sales",
    //             "review" => 4,
    //             "numberOfReviews" => 2,
    //             "slug" => "hightechware",
    //             "totalComments" => 2
    //         ], 
    //         "comment" => [
    //             "author" => [
    //                 "id" => "123",
    //                 "name" => "Luiz Rocha",
    //                 "firstLetter" => "L"
    //             ],
    //             "created_At" => "2021-06-07T12:32:41.8553043Z",
    //             "fullDate" => "7 de junho de 2021 | 12:32",
    //             "updated_At" => "2021-06-14T13:47:00.3189295Z",
    //             "review" => 4,
    //             "title" => "teste123",
    //             "description" => "teste123 ",
    //             "status" => "Approved",
    //             "likes" => 0,
    //             "reply" => [
    //                 "1" => [
    //                     "id" => "60023b2a-40ba-48e7-a4b2-c609660e62e3",
    //                     "description" => "resposta usuario nova",
    //                     "isDraft" => false,
    //                     "author" => null,
    //                     "created_At" => "0001-01-01T00:00:00Z",
    //                     "updated_At" => "2021-06-07T12:42:46.5283074Z",
    //                     "fullDate" => "7 de junho de 2021 | 12:32",
    //                     "isPartner" => true
    //                 ],
    //                 "2" => [
    //                     "id" => "a3e31c89-88c8-4bd5-859b-eee4bd158a7a",
    //                     "description" => "resposta colaborador!",
    //                     "isDraft" => false,
    //                     "author" => null,
    //                     "created_At" => "0001-01-01T00:00:00Z",
    //                     "updated_At" => "2021-06-07T12:39:18.3317356Z",
    //                     "fullDate" => "7 de junho de 2021 | 12:32",
    //                     "isPartner" => false
    //                 ]
    //             ]
                
    //         ]
    //     ]
    // ]);

            //  NEW_COMMENT_APPROVED_PARTNER_PLUGIN
            // Quando um comentario é aprovado, o parceiro tem acesso para responder este comentario. Sendo assim, quando o comentario for aprovado, o parceiro tem que ser notificado sobre o comentario.


    //         return $this->render('emails/plugins/parceiro/new_comment_approved_partner_plugin.html.twig',['obj'=> 
    //     [ 
    //         "id" => "bd2a92ff-183a-45bf-b52e-99889946ca47",
    //         "plugin" => [
    //             "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
    //             "name" => "HighTechWare",
    //             "partnerName" => "Software4Sales",
    //             "review" => 4,
    //             "numberOfReviews" => 2,
    //             "slug" => "hightechware",
    //             "totalComments" => 2
    //         ], 
    //         "comment" => [
    //             "author" => [
    //                 "id" => "123",
    //                 "name" => "Luiz Rocha",
    //                 "firstLetter" => "L"
    //             ],
    //             "created_At" => "2021-06-07T12:32:41.8553043Z",
    //             "fullDate" => "7 de junho de 2021 | 12:32",
    //             "updated_At" => "2021-06-14T13:47:00.3189295Z",
    //             "review" => 4,
    //             "title" => "teste123",
    //             "description" => "teste123 ",
    //             "status" => "Approved",
    //             "likes" => 0,
    //             "reply" => [
    //                 "1" => [
    //                     "id" => "60023b2a-40ba-48e7-a4b2-c609660e62e3",
    //                     "description" => "resposta usuario nova",
    //                     "isDraft" => false,
    //                     "author" => null,
    //                     "created_At" => "0001-01-01T00:00:00Z",
    //                     "updated_At" => "2021-06-07T12:42:46.5283074Z",
    //                     "fullDate" => "7 de junho de 2021 | 12:32",
    //                     "isPartner" => true
    //                 ],
    //                 "2" => [
    //                     "id" => "a3e31c89-88c8-4bd5-859b-eee4bd158a7a",
    //                     "description" => "resposta colaborador!",
    //                     "isDraft" => false,
    //                     "author" => null,
    //                     "created_At" => "0001-01-01T00:00:00Z",
    //                     "updated_At" => "2021-06-07T12:39:18.3317356Z",
    //                     "fullDate" => "7 de junho de 2021 | 12:32",
    //                     "isPartner" => false
    //                 ]
    //             ]
                
    //         ]
    //     ]
    // ]);

    //         NEW_REPLY_EDUZZ_PARTNER
    //         Quando o comentario do usuario é respondido pelo colaborador, o parceiro receba a notificação. Pode ocorrer em dois momentos: quando o comentario é respondido na moderação ou na pagina de detalhes do app.

    //         return $this->render('emails/apps/parceiro/new_reply_eduzz_partner.html.twig',['obj'=> 
    // [ 
    //             "id" => "bd2a92ff-183a-45bf-b52e-99889946ca47",
    //             "app" => [
    //                 "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
    //                 "name" => "HighTechWare",
    //                 "partnerName" => "Software4Sales",
    //                 "review" => 4,
    //                 "numberOfReviews" => 2,
    //                 "slug" => "hightechware",
    //                 "totalComments" => 2
    //             ], 
    //             "comment" => [
    //                 "author" => [
    //                     "id" => "123",
    //                     "name" => "Luiz Rocha",
    //                     "firstLetter" => "L"
    //                 ],
    //                 "created_At" => "2021-06-07T12:32:41.8553043Z",
    //                 "fullDate" => "7 de junho de 2021 | 12:32",
    //                 "updated_At" => "2021-06-14T13:47:00.3189295Z",
    //                 "review" => 4,
    //                 "title" => "teste123",
    //                 "description" => "teste123 ",
    //                 "status" => "Approved",
    //                 "likes" => 0,
    //                 "reply" => [
    //                         "id" => "60023b2a-40ba-48e7-a4b2-c609660e62e3",
    //                         "description" => "resposta usuario nova",
    //                         "isDraft" => false,
    //                         "author" => null,
    //                         "created_At" => "0001-01-01T00:00:00Z",
    //                         "updated_At" => "2021-06-07T12:42:46.5283074Z",
    //                         "fullDate" => "7 de junho de 2021 | 12:32",
    //                         "isPartner" => true
    //                 ]
                    
    //             ]
    //         ]
    //     ]);

       //         NEW_REPLY_EDUZZ_PARTNER_PLUGIN
    //         Quando o comentario do usuario é respondido pelo colaborador, o parceiro receba a notificação. Pode ocorrer em dois momentos: quando o comentario é respondido na moderação ou na pagina de detalhes do app.

    //         return $this->render('emails/plugins/parceiro/new_reply_eduzz_partner_plugin.html.twig',['obj'=> 
    // [ 
    //             "id" => "bd2a92ff-183a-45bf-b52e-99889946ca47",
    //             "plugin" => [
    //                 "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
    //                 "name" => "HighTechWare",
    //                 "partnerName" => "Software4Sales",
    //                 "review" => 4,
    //                 "numberOfReviews" => 2,
    //                 "slug" => "hightechware",
    //                 "totalComments" => 2
    //             ], 
    //             "comment" => [
    //                 "author" => [
    //                     "id" => "123",
    //                     "name" => "Luiz Rocha",
    //                     "firstLetter" => "L"
    //                 ],
    //                 "created_At" => "2021-06-07T12:32:41.8553043Z",
    //                 "fullDate" => "7 de junho de 2021 | 12:32",
    //                 "updated_At" => "2021-06-14T13:47:00.3189295Z",
    //                 "review" => 4,
    //                 "title" => "teste123",
    //                 "description" => "teste123 ",
    //                 "status" => "Approved",
    //                 "likes" => 0,
    //                 "reply" => [
    //                         "id" => "60023b2a-40ba-48e7-a4b2-c609660e62e3",
    //                         "description" => "resposta usuario nova",
    //                         "isDraft" => false,
    //                         "author" => null,
    //                         "created_At" => "0001-01-01T00:00:00Z",
    //                         "updated_At" => "2021-06-07T12:42:46.5283074Z",
    //                         "fullDate" => "7 de junho de 2021 | 12:32",
    //                         "isPartner" => true
    //                 ]
                    
    //             ]
    //         ]
    //     ]);

        // NEW_REPLY_PARTNER_PARTNER
        // Quando o parceiro do aplicativo responde o comentario do usuario, o parceiro é notificado dessa resposta também.

    //     return $this->render('emails/apps/parceiro/new_reply_partner_partner.html.twig',['obj'=> 
    // [ 
    //             "id" => "bd2a92ff-183a-45bf-b52e-99889946ca47",
    //             "app" => [
    //                 "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
    //                 "name" => "HighTechWare",
    //                 "partnerName" => "Software4Sales",
    //                 "review" => 4,
    //                 "numberOfReviews" => 2,
    //                 "slug" => "hightechware",
    //                 "totalComments" => 2
    //             ], 
    //             "comment" => [
    //                 "author" => [
    //                     "id" => "123",
    //                     "name" => "Luiz Rocha",
    //                     "firstLetter" => "L"
    //                 ],
    //                 "created_At" => "2021-06-07T12:32:41.8553043Z",
    //                 "fullDate" => "7 de junho de 2021 | 12:32",
    //                 "updated_At" => "2021-06-14T13:47:00.3189295Z",
    //                 "review" => 4,
    //                 "title" => "teste123",
    //                 "description" => "teste123 ",
    //                 "status" => "Approved",
    //                 "likes" => 0,
    //                 "reply" => [
    //                         "id" => "60023b2a-40ba-48e7-a4b2-c609660e62e3",
    //                         "description" => "resposta usuario nova",
    //                         "isDraft" => false,
    //                         "author" => null,
    //                         "created_At" => "0001-01-01T00:00:00Z",
    //                         "updated_At" => "2021-06-07T12:42:46.5283074Z",
    //                         "fullDate" => "7 de junho de 2021 | 12:32",
    //                         "isPartner" => true
    //                 ]
                    
    //             ]
    //         ]
    //     ]);

    
        // NEW_REPLY_PARTNER_PARTNER_PLUGIN
        // Quando o parceiro do aplicativo responde o comentario do usuario, o parceiro é notificado dessa resposta também.

        return $this->render('emails/plugins/parceiro/new_reply_partner_partner_plugin.html.twig',['obj'=> 
    [ 
                "id" => "bd2a92ff-183a-45bf-b52e-99889946ca47",
                "plugin" => [
                    "icon" => "https://www.hydrocarbons-technology.com/wp-content/uploads/sites/9/2020/09/shutterstock_1152185600-1440x1008-1-857x600.jpg",
                    "name" => "HighTechWare",
                    "partnerName" => "Software4Sales",
                    "review" => 4,
                    "numberOfReviews" => 2,
                    "slug" => "hightechware",
                    "totalComments" => 2
                ], 
                "comment" => [
                    "author" => [
                        "id" => "123",
                        "name" => "Luiz Rocha",
                        "firstLetter" => "L"
                    ],
                    "created_At" => "2021-06-07T12:32:41.8553043Z",
                    "fullDate" => "7 de junho de 2021 | 12:32",
                    "updated_At" => "2021-06-14T13:47:00.3189295Z",
                    "review" => 4,
                    "title" => "teste123",
                    "description" => "teste123 ",
                    "status" => "Approved",
                    "likes" => 0,
                    "reply" => [
                            "id" => "60023b2a-40ba-48e7-a4b2-c609660e62e3",
                            "description" => "resposta usuario nova",
                            "isDraft" => false,
                            "author" => null,
                            "created_At" => "0001-01-01T00:00:00Z",
                            "updated_At" => "2021-06-07T12:42:46.5283074Z",
                            "fullDate" => "7 de junho de 2021 | 12:32",
                            "isPartner" => true
                    ]
                    
                ]
            ]
        ]);
    
    }
}