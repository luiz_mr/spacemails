# Projeto Space E-mails Templates (Front-End)

Esse é o projeto dos templates de e-mails da aplicação da **[Space](https://bitbucket.org/eduzz/space-front/src/master/)**.

## Sumário

-   [Tecnologias Utilizadas](#markdown-header-tecnologias-utilizadas)
-   [Requerimentos de software local](#markdown-header-requerimentos-de-software-local)
-   [Desenvolvimento](#markdown-header-desenvolvimento)
    -   [Primeira instalação](#markdown-header-primeira-instalacao)
    -   [Templates](#templates)
    -   [Branchs](#branchs)

## Tecnologias utilizadas

-   **[PHP 8.0.7](https://windows.php.net/download)**

-   **[Twig](https://twig.symfony.com/)**

## Requerimentos de software local

-   **[Composer version 2.1.3](https://getcomposer.org/download/)**

-   **[XAMPP](https://www.apachefriends.org/pt_br/index.html)**

## Desenvolvimento

### Primeira Instalação

Se você nunca desenvolveu nesse repositório antes precisa seguir os seguintes passos:

1. Instalar o PHP 8.0.7

    - Acesse o link [PHP download](https://windows.php.net/download) e baixe o arquivo .zip (pode ser a versão 8.0.7)
    - Crie uma pasta dentro do Disco Local C: referente ao php e sua versão, por exemplo: php80
    - Extraia a pasta do arquivo .zip para a sua área de trabalho, copie os arquivos que estão dentro desta pasta e cole dentro da "php80"
    - Clique com o botão direito em "Meu computador" e entre em "Propriedades", depois em "Configurações avançadas de sistema", clique na aba "Avançado" e logo em seguida no botão "Variáveis de Ambiente"
    - Em "Variáveis do sistema", selecione a variável "Path" e clique em "Editar". Uma nova janela será aberta, então clique em "Novo" e coloque o caminho da pasta php80: C:\php80. Pronto o PHP está instalado.
    - Para verificar se o php foi instalado corretamente:

        ```bash
        # 1. Verifica a versão do PHP

        $ php -v
        ```

2. Instalar o Composer

    - Para instalar o composer, basta ter o PHP instalado corretamente e após isso, entrar no link abaixo, baixar e instalar o Composer-setuo.exe. Link: [Composer download](https://getcomposer.org/download/)

    - Para verificar se o composer foi instalado corretamente:

        ```bash
        # 1. Verifica a versão do Composer

        $ composer -v
        ```

3. Instalar o XAMPP

    - Para instalar o XAMPP, basta acessar o link abaixo, baixar o executável referente ao seu Windows e a sua versão php instalada e seguir os passos. Após a correta instalação, abra o XAMPP e mantenha o Apache ligado.
      Link: [XAMPP download](https://www.apachefriends.org/pt_br/download.html)

4. Clonar o repositório

```bash
# 1. Clone o repositório dentro da pasta htdocs

$ git clone https://luiz_mr@bitbucket.org/luiz_mr/spacemails.git

# 2 . Acessa a pasta do projeto pelo cmd/terminal

$ cd spacemails

# 3. Instale as dependecias rodando o comando composer install

$ composer install
```

5. Dentro de xampp/apache/conf/extra entra no arquivo httpd-vhosts e adiciona no fim e salva:

    <VirtualHost \*:80>
    DocumentRoot "C:/xampp/htdocs/spacemails/public"
    ServerName spacemails.test
    </VirtualHost>

6. Abre o Bloco de Notas como "Administrador" e clica em "Arquivo" e "Abrir". Busca o arquivo hosts dentro de Windows/System32/drivers/etc. Selecione para mostrar arquivos ocultos e então abra o arquivo "hosts". Dentro de "hosts", escreva "127.0.0.1 spacemails.test" abaixo de Localhost:

    # localhost name resolution is handled within DNS itself.

    # 127.0.0.1 localhost

    # ::1 localhost

    127.0.0.1 spacemails.test

7. Agora é só acessar pelo navegador spacemails.test e conseguirá visualizar o seu template em Twig que está no retorno do EmailTemplateController.

### Templates

Dentro da past templates/emails estão separados os templates de Email das personas abaixo: - Colaborador - Parceiro - Usuário

Para criar um novo template, basta criar um arquivo em uma das três pastas que se referem as personas seguindo o modelo abaixo:

    -> slug.html.twig
    O Slug seria o identifier do template dentro do ContactCenter.

Os templates são baseados no Twig, então eles recebem parâmetros enviados pelo EmailTemplateController.php

Dentro deste controller, você irá criar um novo return passando os parâmetros mockados que seu template irá utilizar, por exemplo:

```bash
        return $this->render('emails/usuarios/new_partner_app.html.twig',['obj'=>
            [
                "app" => [
                    "url" => "https:\\\\www.nutror.com.br",
                    "name" => "Nutror",
                    "description" => "Aplicativo teste"
                ],
                "partner" => [
                    "email" => "teste@gmail.com",
                    "phone" => "14981334567",
                    "name" => "Luiz Rocha",
                    "companyName" => "LCompany"
                ]
                ]]);
```

Quando for testar um novo template, basta comentar os returns que já existem e utilizar o return que você deseja testar no seu template.

### Branchs

Para seguir um padrão de desenvolvimento, crie as branchs seguindo o formato abaixo:

-> template-slug
